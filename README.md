# Love Fraud App

## Create React App
```
npx create-react-app love-fraud-app --use-npm
```

## Create Digital Ocean Project
- Login To Digital Ocean Account
- Created project 'conman'
- Created ubuntu 18 droplet assigned to 157.245.137.158

## Initial Server Setup
Following article at [https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04).

### Log In As Root
```
ssh root@157.245.137.158
```

### Add User
```
adduser mstewart
```

### Grant Admin Privileges
```
usermod -aG sudo mstewart
```

### Setup Basic Firewall
See listed apps.
```
ufw app list
# outputs
Available applications:
  OpenSSH
```

Allow SSH connections through firewall.
```
ufw allow OpenSSH
# outputs
Rules updated
Rules updated (v6)
```

Enable firewall.
```
ufw enable
# outputs
Command may disrupt existing ssh connections. Proceed with operation (y|n)? y
Firewall is active and enabled on system startup
```

See SSH connections still allowed.
```
ufw status
# outputs
Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere                  
OpenSSH (v6)               ALLOW       Anywhere (v6)             

```

### Logout As Root User
```
exit
# outputs
logout
Connection to 157.245.137.158 closed.
```

### Sign In As New User
```
ssh mstewart@157.245.137.158
```

## Install Nginx
Following article at [https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04).

### Update
```
sudo apt update
```

### Install Nginx
```
sudo apt install nginx
```

## Adjust Firewall For Nginx

### List Apps
```
sudo ufw app list
# outputs
Available applications:
  Nginx Full
  Nginx HTTP
  Nginx HTTPS
  OpenSSH
```

### Enable HTTP
```
sudo ufw allow 'Nginx HTTP'
# outputs
Rule added
Rule added (v6)
```

### Verify HTTP Enabled
```
sudo ufw status
# outputs
Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere                  
Nginx HTTP                 ALLOW       Anywhere                  
OpenSSH (v6)               ALLOW       Anywhere (v6)             
Nginx HTTP (v6)            ALLOW       Anywhere (v6)             

```

## Nginx Setup

### Check Status
```
systemctl status nginx
# outputs
● nginx.service - A high performance web server and a reverse proxy 
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendo
   Active: active (running) since Fri 2020-09-18 01:23:59 UTC; 5min 
     Docs: man:nginx(8)
 Main PID: 2594 (nginx)
    Tasks: 2 (limit: 1152)
   CGroup: /system.slice/nginx.service
           ├─2594 nginx: master process /usr/sbin/nginx -g daemon on
           └─2597 nginx: worker process
```
Quit with <kbd>q</kbd>.

### Check Landing Page
Nginx landing page should be visible at [http://157.245.137.158/](http://157.245.137.158/)

## Install Node JS

### Enter Home Dir
```
cd ~
```

### Fetch Node 14 Install Script
```
curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
```

### Run Node Install Script
```
sudo bash nodesource_setup.sh
sudo apt install nodejs
sudo apt install build-essential
```

## Deploy With Git
Following article [https://www.digitalocean.com/community/tutorials/how-to-set-up-automatic-deployment-with-git-with-a-vps](https://www.digitalocean.com/community/tutorials/how-to-set-up-automatic-deployment-with-git-with-a-vps).

### Create Repo
```
cd ~
mkdir repo client && cd repo
mkdir client.git && cd client.git
git init --bare
```

### Add Post Receive Hook
```
cd hooks
cat > post-receive
#!/bin/sh
git --work-tree=/home/mstewart/client --git-dir=/home/mstewart/repo/client.git checkout -f
cd /home/mstewart/client 
npm install
pm2 restart client
```
Stop writing to the file with <kbd>Control + D</kbd>.  
Set permissions:
```
chmod +x post-receive
```

### Add Local Git Remote
```
git remote add live ssh://mstewart@157.245.137.158/home/mstewart/repo/client.git
```

### Push Local To Remote
```
git push live master
```

### Remote NPM Install
```
cd /home/mstewart/client && npm install
```

## Edit Default Server Block
```
sudo nano /etc/nginx/sites-available/default
```
Add the following code to the ```server``` block:
```
    location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
```
### Verify Syntax Valid
```
sudo nginx -t
```

### Restart Nginx
```
sudo systemctl restart nginx
```

## Install Process Manager
```
sudo npm i -g pm2 
```

### Start Client Application With PM2
```
pm2 start npm --name "client" -- start
```